# TP 3 & 4 d'Architectures Distribuées

Vous aurez à rendre ce projet en binôme pour le **8 avril 2021**.
Tout retard sera pénalisé suivant une règle de calcul exponentielle (1 point, 3 points, 6 points, etc.).

Le projet est à rendre par **email** sous la forme d'une adresse vers un **projet GITLAB privé** accessible depuis le web. 
Vous ajouterez vos encadrants (les comptes **ccaron**, **edgou** et **asaval** en tant que développeurs) pour qu'ils puissent avoir accès à votre projet en vous assurant que celui-ci contienne:
1. Les **noms et prénoms** de votre binôme dans un fichier **AUTEURS** à la racine
2. Le **code** intégral commenté
3. Une documentation de votre service dans un fichier **README** à la racine
4. Un **rapport** sur les problèmes rencontrés et les solutions que vous y avez apportés

Vous pouvez également télécharger une version [PDF de l'énoncé du TP](TP-EIP.pdf).
